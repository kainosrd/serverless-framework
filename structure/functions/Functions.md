# Functions folder

Main folder where functions code and deployment code is located.
For each function we need to have two files:
* function-name.py ---> Storing Python code used directly by function
* function-name.sls.yml ---> Storing Serverless deployment code used by this function

## Where other Python code should be used
All code not directly used by the function handler must be located in the subfolders depends of the code purpose. If your code is storing domain model then it should be in folder models. If it is used as a service  allowing access to other components then it should be stored in the services folder. Etc.

## Example
Check folder and hello function for example