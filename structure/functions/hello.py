import logging

from aws_lambda_logging import wrap
from aws_xray_sdk.core import patch_all

patch_all()
logger = logging.getLogger()

import function_shield

function_shield.configure({
    "policy": {
        "outbound_connectivity": "alert",
        "read_write_tmp": "alert",
        "create_child_process": "alert",
        "read_handler": "alert"
    },
    "disable_analytics": True,
    "token": os.environ['FUNCTION_SHIELD_TOKEN']
})

# noinspection PyUnusedLocal
@wrap
def handle(event, context):
    logger.info("Hello world lambda triggered")
    logger.debug(f"Event: {event}")

    response = {
        "statusCode": 200,
        "headers": {"Content-Type": "text/xml"},
        "body": "Hello world",
    }

    return response
