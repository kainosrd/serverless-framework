# Configuration section

In this folder you should store all files responsible for global service configuration like deployment bucket name or api gateway configuration.

All files must contains sufix .sls.yml
It will indicate that those are Serverless Framework files in the yaml format.

## Examples
Check this folder for examples