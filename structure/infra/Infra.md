# Infra folder
The purpose of this folder is to store all custom resources created using AWS CloudFormation.

If you require a resources which are not automaticaly created by the Serverless Framework then you can create them using CloudFormation templates.

The most common example is DynamoDB.
