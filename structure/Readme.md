# Advanced project structure

This is a example of folders structure used in non trivial serverless projects.
It is seperating components responsibilities into three main areas:
* Global configuration (folder config)
* Functions business logic and deployment code (folder functions)
* Infrastructure components created using CloudFormation (folder infra)

The entry point is in the serverless.yml file.