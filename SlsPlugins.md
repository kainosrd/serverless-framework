# Serverless Framework plugins

Serverless framework has impresive list of plugins delivered by the community.

In our AI projects we are using:
- serverless-iam-roles-per-function
The most important plugin allowing to set privilages per function, not to the whole service. It is important from security point of view because all functions are not able to do more then they require to normal wor.

- serverless-domain-manager
Usefull pluging allowing to automaticaly set a domain to the Api Gateway. It must be initiated on the  account before first deploying using new domain.
More in the documentation: ToDo

- serverless-python-requirements
Library responsible for building python dependencies. It is using Docker image similar to the environment used by AWS Lambda.

- serverless-s3-remover
This is a riski plugin because it can remove files from S3 and should be used only on development account. If  used unproperly it can cause data loses. If you want to remove environment then you need to remove S3 bucket, but it is not possible to remove bucket with files and here this plugin is usefull.

- serverless-deployment-bucket
Plugin is extending Serverless configutation options by allowing to set custom bucket name and bucket data encription.

- serverless-pseudo-parameters
Advanced plugin used by the resources located in the infra folder

- serverless-plugin-lambda-dead-letter
It is extending error handling by configuring SQS service and setting death leather queue on the function. This queue will contain all event payload causing function crashes.