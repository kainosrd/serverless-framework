First steps at https://github.com/Marcin-Duszynski/IntroductionToServerless-workshops
Most uptodate version of this repo at https://gitlab.kainos.com/M.Duszynski/serverless-recipes

# Serverless recipes
Work in progress

This repository contains a set of Serverless examples describing how to create production-ready SLS components.

## Getting started example
[Getting started  using Python](https://serverless.com/framework/docs/providers/aws/examples/hello-world/python/)

## Advanced project structure
[Go to](/structure/Readme.md)
